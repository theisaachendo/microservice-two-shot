from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import BinVO, Shoes


class BinEncoder(ModelEncoder):
    model = bin
    properties = [
        "id",
        "closet_name",
        "bin_number",
        "bin_size",
    ]

class ShoeEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "model_name",
        "manufactuurer",
        "color",
        "picture_url",
    ]


@require_http_methods(["GET", "POST"])
def api_shoes(request):
    if request.method == "GET":
        shoes = Shoes.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeEncoder,
        )
    else:
        content = json.loads(request.body)
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeEncoder,
            safe=False,
        )
